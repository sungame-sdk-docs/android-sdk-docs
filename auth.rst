Authentication
=======================================================

Login and Register
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Call this method to show the authentication screen

.. code-block:: java
    
        // authenticationCallBack is interface which have 2 call back for login and register
        // loginCallback(authenticationResult, provider) see funcation's docs
        // registerCallback(authenticationResult) see funcation's docs
        SunGameSDK.showLogin(authenticationCallBack)

which callback is:
        
.. code-block:: kotlin
    
        interface AuthenticationCallBack {
            /**
             * Call back when login with or without sns finish
             * @param authenticationResult: if succeed authentication info can be obtained by authenticationResult.data() else error can be obtained by authenticationResult.error()
             * @param provider: if login without sns then it's null else it's string of provider for ex: "google" or "facebook"
             */
            fun onLoginResult(authenticationResult: WorkResult<AuthenticationInfo>, provider: String?)

            /**
             * Call back when login with or without sns finish
             * @param authenticationResult: if succeed authentication info can be obtained by authenticationResult.data() else error can be obtained by authenticationResult.error()
             */
            fun onRegisterResult(authenticationResult: WorkResult<AuthenticationInfo>)
        }

and WorkResult is:

.. code-block:: kotlin

        data class WorkResult<out T>(
            //[data] if succeed
            internal val data: T? = null, 
            //[error] if any error occurs
            internal val error: SdkError? = null
        ) 

with SdkError:


.. code-block:: kotlin

        class SdkError(
            // internal Sdk defined code
            val code: Int,
            // the error message
            message: String,
            // the origin error
            cause: Throwable? = null
        ) : Throwable(message, cause)
    
       
