Installation (Step by step)
====================================================================
Import sdk
"""""""""""""""""

This can be done by 2 way:

- Import the sdk .aar file as `this guide <https://developer.android.com/studio/projects/android-library>`_

- Copy sdk <sdk-release>.aar file to <app or game>/libs folder and then make sure build.gradle the app or game's module includes this line


.. code-block:: 
    
    // At top of file
    apply plugin: 'kotlin-android'
    apply plugin: 'kotlin-kapt'
    apply plugin: 'kotlin-android-extensions'
    apply plugin: 'realm-android'
    
    // At dependency section
    implementation(name:'sdk-release', ext:'aar')
    implementation "org.jetbrains.kotlin:kotlin-stdlib:1.3.72"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.9"

    implementation "com.github.vicpinm:krealmextensions:2.5.0"
    implementation "androidx.core:core-ktx:1.3.1"
    implementation "com.squareup.retrofit2:retrofit:2.9.0"
    implementation "com.squareup.retrofit2:converter-gson:2.9.0"
    implementation "com.squareup.okhttp3:okhttp:4.3.0"
    implementation "com.squareup.okhttp3:logging-interceptor:4.3.0"

    implementation "androidx.appcompat:appcompat:1.2.0"
    implementation "com.google.android.material:material:1.2.1"

    implementation 'com.facebook.android:facebook-android-sdk:7.1.0'
    implementation 'com.google.android.gms:play-services-auth:17.0.0'

    implementation platform('com.google.firebase:firebase-bom:26.2.0')
    implementation 'com.google.firebase:firebase-auth-ktx'
    implementation 'com.google.firebase:firebase-auth'
    implementation "com.google.firebase:firebase-messaging"
    implementation "com.android.installreferrer:installreferrer:2.2"
    implementation 'com.appsflyer:af-android-sdk:6.1.1'

    implementation 'org.solovyev.android:checkout:1.2.2'

    implementation "com.jakewharton.timber:timber:4.7.1"
    Implementation 'com.appsflyer:af-android-sdk:6.1.1'
    Implementation 'com.bugfender.sdk:android:3.+'
    
    //At the end of file 
    apply plugin: 'com.google.gms.google-services'
        

- And at build.gradle of root project:

.. code-block:: 

        buildscript {
            ext.kotlin_version = "1.3.72"
            ext.butterknifeLatestReleaseVersion = "10.2.1"
            repositories {
                google()
                jcenter()
                mavenCentral()
                maven {
                    url "https://maven.fabric.io/public"
                }
                maven {
                    url "https://plugins.gradle.org/m2/"
                }
            }
            dependencies {
                classpath "com.android.tools.build:gradle:4.0.1"
                classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
                classpath "io.realm:realm-gradle-plugin:6.0.2"
                classpath "com.jakewharton:butterknife-gradle-plugin:$butterknifeLatestReleaseVersion"
                classpath "com.google.gms:google-services:4.3.4"
                classpath 'com.google.firebase:firebase-crashlytics-gradle:2.2.0'
                // NOTE: Do not place your application dependencies here; they belong
                // in the individual module build.gradle files
            }
        }

        allprojects {
                // Other repository
                // ...
                flatDir {
                    dirs 'libs'
                }
            }
        }
        
Configuration
"""""""""""""""""
- Resource configuration:

    + Add and change sdk_conf.xml 's value base on your config into <app>/src/<*>/res/values:

- In your app initialization code (make sure your class in your <application android:name="<your app class name>">"

.. code-block::
    
        class <your app class name> : Application() {
            override fun onCreate() {
                super.onCreate()
                SunGameSDK.init(this)
                // Other init code
            }
        }            }    
