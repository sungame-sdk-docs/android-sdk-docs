Other methods
=======================================================

Logout
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use this method to clear the saved token and facebook credential when user want to switch to new account

.. code-block:: java

        SunGameSDK.logOut()

Use this method to get stored ApiToken & stored uuid (return null if user haven't login yet)

.. code-block:: java

        SunGameSDK.getAuthenticationInfo()


Use this method to log event to Appflyer

.. code-block:: java

        SunGameSDK.logEvent(String eventType, Map<String, *> eventData)