Payment
=======================================================

IAP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Call this method to start the payment process

.. code-block:: java
    
        // gameData: an object contain user information
        // callback: a callback return the status and data of IAP flow
        SunGameSDK.showPayment(gameData: GameData, callback: IabCallBack)

.. code-block:: kotlin

        // the content of this class is depended on the partner
        @Parcelize
        open class GameData(
            @SerializedName("server_id")
            val serverId: Int,
            @SerializedName("level")
            val level: Int,
            @SerializedName("role_id")
            val roleId: Int,
            @SerializedName("account_id")
            val accountId: Int = 0
        ) : Parcelable
        
.. code-block:: java

        interface IabCallBack {
            /**
             * Call back when purchase finish
             * @param iabResult: if succeed purchase info can be obtained by iabResult.data() else error can be obtained by iabResult.error(). Purchase information as described <a href="http://developer.android.com/google/play/billing/billing_reference.html#purchase-data-table">here</a>
             */
            fun onIabResult(iabResult: WorkResult<PurchaseInfo>)
        }
