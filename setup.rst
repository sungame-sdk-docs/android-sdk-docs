Prerequisite
=======================================================

Dependency
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The SDK include several library. They are:

- Kotlin standard and kotlin coroutine library
- Firebase & Play service SDK
- Realm library
- Okhttp and retrofit library
- Android standard and android x library
- Timber library
- Android checkout library
- Appflyer sdk

Required Information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Some information we must provided for the partner to use. Please ask for it if you don't see it. They are:

- Facebook config gile sdk_conf.xml
- Firebase google-services.json file
- SDK workflow and server api documentation
- Android keystore and password for it

Services
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
These Services may need to be enabled to allow the SDK to function

- Android In app Purchase
- Firebase
- Facebook Login
